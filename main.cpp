#include <iostream>
#include <string>
#include <algorithm>


int main()
{
    try
    {
        /* https://adventofcode.com/2019/day/5
        */
        std::cout << "init"<< std::endl;
    }
    catch (const std::exception &e)
    {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    return 0;
}
